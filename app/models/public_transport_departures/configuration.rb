# frozen_string_literal: true

module PublicTransportDepartures
  class Configuration < WidgetInstanceConfiguration
    attribute :show_header, :boolean, default: true
    attribute :show_station_name, :boolean, default: true

    validates :show_header, :show_station_name, boolean: true
  end
end
