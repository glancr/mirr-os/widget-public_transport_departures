require 'json'
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require 'public_transport_departures/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "public_transport_departures"
  s.version     = PublicTransportDepartures::VERSION
  s.authors     = ["Tobias Grasse"]
  s.email       = ["tg@glancr.de"]
  s.homepage    = "https://glancr.de/modules/public_transport_departures"
  s.summary     = "mirr.OS widget that shows public transport departure times."
  s.description = "Shows you current departure times at a public transport station of your choice."
  s.license     = "MIT"
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Departures',
                      deDe: 'ÖPNV',
                      frFr: 'Départs',
                      esEs: 'Salidas',
                      plPl: 'Odloty',
                      koKr: '출발'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Zeigt dir die aktuellen Abfahrtszeiten einer ÖPNV-Station deiner Wahl.',
                      frFr: 'Affiche les heures de départ actuelles dans une station de transport en commun de votre choix.',
                      esEs: 'Muestra los horarios de salida actuales en una estación de transporte público de su elección.',
                      plPl: 'Pokazuje aktualne godziny odjazdów na wybranej stacji transportu publicznego.',
                      koKr: '선택한 대중 교통 역에서 현재 출발 시간을 보여줍니다.'
                    },
                    sizes: [
                      {
                        w: 6,
                        h: 4
                      },
                      {
                        w: 4,
                        h: 4
                      }
                    ],
                    languages: [:enGb, :deDe, :frFr, :esEs, :plPl, :koKr],
                    group: 'public_transport',
                    compatibility: '0.13.0',
                    single_source: true
                  }.to_json
                }

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_development_dependency 'rails'
end
