module PublicTransportDepartures
  class Engine < ::Rails::Engine
    isolate_namespace PublicTransportDepartures
    config.generators.api_only = true
  end
end
