# frozen_string_literal: true

module PublicTransportDepartures
  VERSION = '0.3.0'
end
