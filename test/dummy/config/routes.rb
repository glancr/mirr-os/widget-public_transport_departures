Rails.application.routes.draw do
  mount PublicTransportDepartures::Engine => "/public_transport_departures"
end
